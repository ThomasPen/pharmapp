/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
/* To start the app : react-native run-android */

import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollableTabBar} from './Components/ScrollableTabBar';

import Store from './Store/Store';
import {Provider} from 'react-redux';
import SearchBar from './Components/SearchBar';

export class App extends React.Component {
  render() {
    return (
      <Provider store={Store}>
        <View style={styles.app}>
          <SearchBar />
          <ScrollableTabBar />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  app: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
});
