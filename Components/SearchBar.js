import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import fetchGeocodeAction from '../Store/Actions/fetchGeocode';
import {
  getGeocode,
  getGeocodeError,
  getGeocodePending,
  getPharmacies,
} from '../Store/Reducers/Reducer';
import {default_location} from '../default_values/default_location';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentRender = this.shouldComponentRender.bind(this);
  }

  componentDidMount() {
    const {fetchSuccess} = this.props;
    console.log('component did mount');
    fetchSuccess(default_location);
  }

  //unused
  shouldComponentRender() {
    const {pending} = this.props;
    if (this.pending === false) return false;
    return true;
  }

  _searchBar_onInput = obj => {
    const {fetchSuccess} = this.props;
    let text = obj.nativeEvent.text;
    let address = text
      .replace(/\s+/g, ' ')
      .trim()
      .replace(/ /g, '+');
    fetchSuccess(address);
  };

  render() {
    const {geocode, error, pending, pharmacies} = this.props;
    //console.log('SearchBar');
    return (
      <TextInput
        style={styles.searchbar}
        editable
        placeholder={'Enter an address here'}
        onSubmitEditing={obj => this._searchBar_onInput(obj)}
      />
    );
  }
}

type State = NavigationState<{
  key: string,
  title: string,
}>;

const styles = StyleSheet.create({
  searchbar: {
    height: '10%',
    width: '100%',
    padding: '1%',
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  error: getGeocodeError(state),
  geocode: getGeocode(state),
  pharmacies: getPharmacies(state),
  pending: getGeocodePending(state),
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchSuccess: fetchGeocodeAction,
    },
    dispatch,
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchBar);
