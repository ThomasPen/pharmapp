import React from 'react';
import {StyleSheet} from 'react-native';
import {
  SceneMap,
  SceneRendererProps,
  TabBar,
  TabView,
} from 'react-native-tab-view';
import GoogleMaps from './GoogleMaps';
import PharmList from './PharmList';

export class ScrollableTabBar extends React.Component<{}, State> {
  static title = 'Scrollable tab bar';

  state = {
    index: 1,
    routes: [{key: 1, title: 'PharMap'}, {key: 2, title: 'PharmList'}],
  };

  renderScene = SceneMap({
    1: GoogleMaps,
    2: PharmList,
  });

  handleIndexChange = (index: number) =>
    this.setState({
      index,
    });

  renderTabBar = (props: SceneRendererProps & {navigationState: State}) => (
    <TabBar
      {...props}
      scrollEnabled
      swipeEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this.renderScene}
        renderTabBar={this.renderTabBar}
        onIndexChange={this.handleIndexChange}
      />
    );
  }
}

const styles = StyleSheet.create({
  tabbar: {
    backgroundColor: '#3f51b5',
  },
  tab: {
    width: 120,
  },
  indicator: {
    backgroundColor: 'lightblue',
  },
  label: {
    fontWeight: '400',
  },
});
