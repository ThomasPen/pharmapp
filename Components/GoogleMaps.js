import React from 'react';
import {StyleSheet, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

import {connect} from 'react-redux';
import {
  getGeocode,
  getGeocodeError,
  getGeocodePending,
  getPharmacies,
} from '../Store/Reducers/Reducer';
import {bindActionCreators} from 'redux';
import fetchGeocodeAction from '../Store/Actions/fetchGeocode';
import pharmacy_icon from '../ressources/pharmacy_icon.png';

const geodata_to_location = geodata => ({
  latitude: geodata.geometry.location.lat,
  longitude: geodata.geometry.location.lng,
});

class GoogleMaps extends React.Component {
  constructor(props) {
    super(props);
  }

  // Create a list of markers from the pharmData we got from GoogleAPI and that we cleaned beforehand.
  _pharmacies_to_markers(pharmacies) {
    let name = this.props.geocode.name;
    let latitude = this.props.geocode.latitude;
    let longitude = this.props.geocode.longitude;
    let payload = pharmacies.map(item => {
      return (
        <Marker
          key={item.name}
          coordinate={geodata_to_location(item)}
          title={
            '[' + (item.opening_hours.open_now ? 'O' : 'F') + '] ' + item.name
          }
          description={
            'rating: ' +
            (typeof item.rating === 'undefined' ? '' : item.rating.toString())
          }
          image={pharmacy_icon}
        />
      );
    });
    // on ajoute le marqueur de la position actuelle à la fin.
    payload.push(
      <Marker
        key={name}
        coordinate={{
          latitude: latitude,
          longitude: longitude,
        }}
        title={name}
        description={''}
      />,
    );
    return payload;
  }

  render() {
    //console.log('GoogleMaps');
    return (
      <View style={styles.container}>
        {this.props.geocode ? (
          <MapView
            style={styles.map}
            region={{
              ...this.props.geocode,
              latitudeDelta: 0.005,
              longitudeDelta: 0.015,
            }}>
            {this._pharmacies_to_markers(this.props.pharmacies)}
          </MapView>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  map: {
    height: '100%',
    width: '100%',
  },
});

const mapStateToProps = state => ({
  error: getGeocodeError(state),
  geocode: getGeocode(state),
  pharmacies: getPharmacies(state),
  pending: getGeocodePending(state),
});
//const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);
export default connect(mapStateToProps)(GoogleMaps);
