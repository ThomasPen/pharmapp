import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {
  getGeocode,
  getGeocodeError,
  getGeocodePending,
  getPharmacies,
} from '../Store/Reducers/Reducer';
const placeholder = require('./../ressources/image-placeholder.jpg');
const topbar_icons = [
  require('./../ressources/baseline_directions_bike_black_18dp.png'),
  require('./../ressources/baseline_directions_car_black_18dp.png'),
  require('./../ressources/baseline_directions_railway_black_18dp.png'),
  require('./../ressources/baseline_directions_walk_black_18dp.png'),
];
const stars_icons = [
  require('./../ressources/baseline_star_black_18.png'),
  require('./../ressources/baseline_star_half_black_18.png'),
  require('./../ressources/baseline_star_border_black_18.png'),
];
const addon_icons = [
  require('./../ressources/baseline_directions_black_18.png'),
  require('./../ressources/baseline_public_black_18.png'),
  require('./../ressources/baseline_local_phone_black_18.png'),
  require('./../ressources/baseline_directions_grey_18.png'),
  require('./../ressources/baseline_public_grey_18.png'),
  require('./../ressources/baseline_local_phone_grey_18.png'),
];
const google_directions = 'https://www.google.com/maps/dir/';

// missing link to googlemap comments
const Review = (value, count) => {
  let list = [];
  let source = '';
  for (let i = 0; i < 5; i++) {
    source = '';
    if (value > i && value < i + 1) source = stars_icons[1];
    else if (value > i) source = stars_icons[0];
    else source = stars_icons[2];
    list.push(<Image source={source} style={styles.PharmReviewStarsImage} />);
  }
  return (
    <View style={styles.PharmReviewContainer}>
      <Text style={styles.PharmReviewText}>{value.toString()}</Text>
      <View style={styles.PharmReviewStarsContainer}>{list}</View>
      <Text style={styles.PharmReviewText}>{'(' + count.toString() + ')'}</Text>
    </View>
  );
};

const mode = ['bicycling', 'driving', 'transit', 'walking'];
const Transport = (type, time) => (
  <View style={styles.PharmTransportContainer}>
    <Image source={topbar_icons[type]} style={styles.PharmTransportIcon} />
    <Text style={styles.PharmTransportText}>{time.toString() + ' min.'}</Text>
  </View>
);

// missing every links and checking if info is available

const BaseInfo = PharmData => (
  <View style={styles.PharmInfoContainer}>
    <Text style={styles.PharmInfoTitle} numberOfLines={3}>
      {PharmData.name}
    </Text>
    <TouchableOpacity
      onPress={() => {
        Alert.alert(
          'Horaires',
          PharmData.details.opening_hours.weekday_text.join('\n'),
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        );
      }}>
      {PharmData.opening_hours.open_now ? (
        <Text style={styles.PharmOpen}>{'OUVERT'}</Text>
      ) : (
        <Text style={styles.PharmClosed}>{'FERME'}</Text>
      )}
    </TouchableOpacity>
    {Review(PharmData.rating, PharmData.user_ratings_total)}
  </View>
);

class PharmList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transport_selected: 0,
    };
  }

  Addon = PharmData => {
    let latitude_src = this.props.geocode.latitude;
    let longitude_src = this.props.geocode.longitude;
    let list = [];
    let tagValue = [
      google_directions +
        latitude_src +
        '+' +
        longitude_src +
        '/' +
        PharmData.geometry.location.lat +
        '+' +
        PharmData.geometry.location.lng,
      PharmData.details.website,
      'tel:${' +
        (PharmData.details.international_phone_number ||
          PharmData.details.formatted_phone_number) +
        '}',
    ];
    let grey = 0;
    for (let i = 0; i < 3; i++) {
      grey = typeof tagValue[i] === 'undefined' ? 3 : 0;
      list.push(
        <TouchableOpacity
          onPress={
            grey === 3
              ? () => {}
              : () => {
                  Linking.openURL(tagValue[i]);
                }
          }>
          <Image source={addon_icons[i + grey]} style={styles.PharmAddonIcon} />
        </TouchableOpacity>,
      );
    }
    return <View style={styles.PharmAddonContainer}>{list}</View>;
  };

  //missing selecting of transport
  Topbar = imgList => {
    let list = [];
    for (let i = 0; i < imgList.length; i++) {
      list.push(
        <TouchableOpacity
          key={'topbarTouch' + i}
          onPress={() => {
            let state = this.state;
            this.setState({...this.state, transport_selected: i});
          }}>
          <Image
            key={'topbarImage' + i}
            source={imgList[i]}
            style={styles.topbarIcon}
          />
        </TouchableOpacity>,
      );
    }
    return <View style={styles.topbarContainer}>{list}</View>;
  };

  _render_pharmacies(pharmData) {
    let list = [];
    for (let i = 0; i < pharmData.length; i++) {
      list.push(
        <View style={styles.PharmElement}>
          <Image style={styles.PharmImage} source={placeholder} />
          {BaseInfo(pharmData[i])}
          <View style={styles.PharmLinksContainer}>
            {Transport(
              this.state.transport_selected,
              Math.floor(
                pharmData[i].details[mode[this.state.transport_selected]] / 60,
              ),
            )}
            <View style={styles.separator} />
            {this.Addon(pharmData[i])}
          </View>
        </View>,
      );
    }
    return (
      <View style={styles.containerPharmElement}>
        <ScrollView vertical style={styles.containerScrollViewPharmElement}>
          {list}
        </ScrollView>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.Topbar(topbar_icons)}
        {this.props.pharmacies
          ? this._render_pharmacies(this.props.pharmacies)
          : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    backgroundColor: 'grey',
  },
  containerPharmElement: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'lightblue',
    height: '100%',
  },
  containerScrollViewPharmElement: {},
  topbarIcon: {
    marginLeft: 15,
    marginRight: 15,
    height: 40,
    width: 40,
  },
  topbarContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'grey',
    width: '100%',
    height: '10%',
    backgroundColor: 'white',
  },
  PharmElement: {
    display: 'flex',
    flexDirection: 'row',
    height: 120,
    width: '100%',
    borderWidth: 1,
    borderColor: 'lightgrey',
    backgroundColor: 'white',
    padding: '3%',
    justifyContent: 'space-between',
  },
  PharmImage: {
    height: '100%',
    width: '25%',
    marginBottom: 'auto',
    marginTop: 'auto',
  },
  PharmInfoContainer: {
    width: '45%',
    display: 'flex',
    textAlign: 'center',
    marginLeft: '3%',
    marginRight: '3%',
  },
  PharmInfoTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    flexShrink: 1,
    flex: 1,
    flexWrap: 'wrap',
  },
  PharmOpen: {
    marginTop: '1%',
    marginBottom: '1%',
    color: 'lightgreen',
    fontWeight: 'bold',
  },
  PharmClosed: {
    color: 'red',
    fontWeight: 'bold',
  },
  PharmReviewContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  PharmReviewText: {
    fontSize: 14,
  },
  PharmReviewStarsContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  PharmReviewStarsImage: {
    height: 15,
    width: 15,
  },
  PharmLinksContainer: {
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    width: '30%',
  },
  PharmTransportContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
  },
  PharmTransportIcon: {
    height: 30,
    width: 30,
  },
  PharmTransportText: {
    fontSize: 12,
  },
  separator: {
    borderWidth: 1,
    borderColor: 'grey',
    margin: 5,
  },
  PharmAddonContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  PharmAddonIcon: {
    height: 25,
    width: 25,
  },
});

const mapStateToProps = state => ({
  error: getGeocodeError(state),
  geocode: getGeocode(state),
  pharmacies: getPharmacies(state),
  pending: getGeocodePending(state),
});
//const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);
export default connect(mapStateToProps)(PharmList);
