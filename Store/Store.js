import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import {Reducer} from './Reducers/Reducer';
const initialState = require('../default_values/default_state.json');

const middlewares = [thunk];
export default createStore(
  Reducer,
  initialState,
  applyMiddleware(...middlewares),
);
