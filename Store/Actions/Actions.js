export const UPDATE_STATE = 'UPDATE_STATE';

export const FETCH_PENDING = 'FETCH_GEOCODE_PENDING';
export const FETCH_GEOCODE_SUCCESS = 'FETCH_GEOCODE_SUCCESS';
export const FETCH_ERROR = 'FETCH_ERROR';

export function fetchPending() {
  return {
    type: FETCH_PENDING,
  };
}

export function fetchSuccess(payload) {
  let geocode = payload.geocode;
  let pharmacies = payload.pharmacies;
  //console.log('fetchSuccess');
  return {
    type: FETCH_GEOCODE_SUCCESS,
    geocode: geocode,
    pharmacies: pharmacies,
  };
}

export function fetchError(error) {
  return {
    type: FETCH_ERROR,
    error: error,
  };
}
