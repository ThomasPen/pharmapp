import {fetchPending, fetchSuccess, fetchError} from './Actions';
import {API_KEY} from '../../default_values/API_KEY';
import {
  clean_fetchGeocode,
  clean_pharmacies,
  clean_pharmacy_data,
} from '../../GoogleRequests/clean_pharmacies';

function fetchDirections(i, payload, callback, mode) {
  let mode_list = ['walking', 'driving', 'bicycling', 'transit'];
  let latitude_src = payload.geocode.latitude;
  let longitude_src = payload.geocode.longitude;
  let latitude_dest = payload.pharmacies[i].geometry.location.lat;
  let longitude_dest = payload.pharmacies[i].geometry.location.lng;
  let url =
    'https://maps.googleapis.com/maps/api/directions/json?mode=' +
    mode_list[mode] +
    '&origin=' +
    latitude_src +
    '+' +
    longitude_src +
    '&destination=' +
    latitude_dest +
    '+' +
    longitude_dest +
    '&key=' +
    API_KEY;
  fetch(url)
    .then(res => res.json())
    .then(res => {
      payload.pharmacies[i].details[mode_list[mode]] =
        res.routes[0].legs[0].duration.value;
      if (mode === 0 && i === 0) {
        callback(fetchSuccess(payload));
        return payload;
      } else if (mode === 0) {
        fetchDirections(i - 1, payload, callback, 3);
      } else {
        fetchDirections(i, payload, callback, mode - 1);
      }
    })
    .catch(error => {
      callback(fetchError(error));
    });
}

function _fetchPharmacyDetails(i, payload, callback) {
  let url =
    'https://maps.googleapis.com/maps/api/place/details/json?place_id=' +
    payload.pharmacies[i].place_id +
    '&key=' +
    API_KEY;
  fetch(url)
    .then(res => res.json())
    .then(res => {
      payload.pharmacies[i].details = clean_pharmacy_data(res.result);
      if (i !== 0) {
        _fetchPharmacyDetails(i - 1, payload, callback);
      } else {
        return fetchDirections(
          payload.pharmacies.length - 1,
          payload,
          callback,
          3,
        );
      }
    })
    .catch(error => {
      callback(fetchError(error));
    });
}
function fetchPharmacyDetails(payload, callback) {
  if (!payload.pharmacies) {
    callback(fetchError('no pharmacies'));
    return payload;
  }
  _fetchPharmacyDetails(payload.pharmacies.length - 1, payload, callback);
}

function fetchPharmacies(payload, callback) {
  let latitude = payload.latitude;
  let longitude = payload.longitude;
  const url =
    'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' +
    'location=' +
    latitude +
    ',' +
    longitude +
    '&radius=1000&type=pharmacy&key=' +
    API_KEY;
  fetch(url)
    .then(res => res.json())
    .then(res => {
      let pharmacies = clean_pharmacies(res.results);
      return fetchPharmacyDetails(
        {geocode: payload, pharmacies: pharmacies},
        callback,
      );
    })
    .catch(error => {
      callback(fetchError(error));
    });
}

function fetchGeocode(address) {
  //console.log('fetchGeocode');
  const url =
    'https://maps.google.com/maps/api/geocode/json?address=' +
    address +
    '&key=' +
    API_KEY;
  return dispatch => {
    dispatch(fetchPending());
    fetch(url)
      .then(res => res.json())
      .then(res => {
        if (res.error) {
          throw res.error;
        }
        let payload = clean_fetchGeocode(res.results[0]);
        return fetchPharmacies(payload, dispatch);
      })
      .catch(error => {
        dispatch(fetchError(error));
      });
  };
}

export default fetchGeocode;
