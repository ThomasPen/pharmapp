import {UPDATE_STATE} from '../Actions/Actions';

import {
  FETCH_PENDING,
  FETCH_GEOCODE_SUCCESS,
  FETCH_ERROR,
} from '../Actions/Actions';

const initialState = {
  pending: false,
  geocode: {},
  pharmacies: [],
  error: null,
};

export function Reducer(state = initialState, action) {
  //console.log('Reducer');
  switch (action.type) {
    case FETCH_PENDING:
      console.log('FETCH_PENDING');
      return {
        ...state,
        pending: true,
      };
    case FETCH_GEOCODE_SUCCESS:
      console.log('FETCH_GEOCODE_SUCCESS');
      return {
        ...state,
        pending: false,
        geocode: action.geocode,
        pharmacies: action.pharmacies,
      };
    case FETCH_ERROR:
      console.log('FETCH_ERROR');
      console.log(action.error);
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getGeocode = state => state.geocode;
export const getGeocodePending = state => state.pending;
export const getGeocodeError = state => state.error;

export const getPharmacies = state => state.pharmacies;
