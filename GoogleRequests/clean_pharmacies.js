export const clean_pharmacy_data = pharmData => {
  return {
    formatted_phone_number: pharmData.formatted_phone_number
      ? pharmData.formatted_phone_number
      : pharmData.international_phone_number,
    international_phone_number: pharmData.international_phone_number,
    opening_hours: pharmData.opening_hours
      ? {
          open_now: pharmData.opening_hours.open_now
            ? pharmData.opening_hours.open_now
            : false,
          periods: pharmData.opening_hours.periods
            ? pharmData.opening_hours.periods
            : [],
          weekday_text: pharmData.opening_hours.weekday_text
            ? pharmData.opening_hours.weekday_text
            : '',
        }
      : undefined,
    website: pharmData.website,
    photos: pharmData.photos,
  };
};

export const clean_pharmacies = pharmData => {
  return pharmData.map(item => {
    return {
      name: item.name,
      geometry: {
        location: {
          lat: item.geometry.location.lat,
          lng: item.geometry.location.lng,
        },
      },
      rating: item.rating || 0,
      opening_hours:
        item.opening_hours && item.opening_hours.open_now
          ? {open_now: item.opening_hours.open_now}
          : {open_now: false},
      user_ratings_total: item.user_ratings_total || 0,
      place_id: item.place_id,
    };
  });
};

export const clean_fetchGeocode = results => {
  return {
    latitude: results.geometry.location.lat,
    longitude: results.geometry.location.lng,
    name: results.formatted_address,
  };
};
